import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Gymlist from './views/Gymlist'
import Nest from './views/Nest'
import Friendlist from './views/Friendlist'
import Pokestop from './views/Pokestop'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes:[
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/gymlist',
            name: 'gymlist',
            component: Gymlist
        },
        {
            path: '/nest',
            name: 'nest',
            component: Nest
        },
        {
            path: '/friendlist',
            name: 'friendlist',
            component: Friendlist
        },
        {
            path: "/pokestop",
            name: "pokestop",
            component: Pokestop
        }
    ]
})
